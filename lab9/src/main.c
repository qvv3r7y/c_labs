#include "dijkstra_path.h"

#define MAX_QTY_VERT 5000

static inline const char *get_msg(uint8_t code) {
    return (code < MAX_ERROR_MSG) ? error_msg[code] : error_msg[0];
}

static void print_path(vertex_t *graph, short from, short to) {
    printf("%d ", to);
    while (from != to) {
        to = graph[to].parent;
        printf("%d ", to);
    }
}

static void print_cost_path(vertex_t *graph, short qty_vert, short from, short to) {
    for (int i = 1; i <= qty_vert; ++i) {
        if (graph[i].parent == -1) printf("%c%c ", 'o', 'o');
        else if (graph[i].min_cost > INT_MAX) printf("%s ", "INT_MAX+");
        else printf("%u ", graph[i].min_cost);
    }
    printf("\n");
    if (graph[from].parent == -1 || graph[to].parent == -1) printf("no path");
    else if (graph[to].min_cost > INT_MAX && graph[to].qty_in_path > 1) printf("overflow");
    else print_path(graph, from, to);
}

int main(void) {
    uint8_t retcode = 0;
    short qty_vert = 0, vert_from = 0, vert_to = 0;
    int32_t qty_edg = 0;
    scanf("%hd", &qty_vert);
    scanf("%hd %hd", &vert_from, &vert_to);
    scanf("%d", &qty_edg);
    if (qty_vert > MAX_QTY_VERT || qty_vert < 0) retcode = QTY_VERT_ERR;
    if (qty_edg > (qty_vert * (qty_vert - 1) / 2) || qty_edg < 0) retcode = QTY_EDGES_ERR;
    if (vert_from > qty_vert || vert_from < 1 || vert_to > qty_vert || vert_to < 1) retcode = IN_VERT_ERR;
    if (retcode) {
        fprintf(stdout, "%s", get_msg(retcode));
        return 0;
    }

    vertex_t *graph = calloc((size_t) qty_vert + 1, sizeof(vertex_t));
    if (!graph) {
        fprintf(stderr, "%s", get_msg(ALLOC_ERR));
        return ALLOC_ERR;
    }

    for (int i = 0; i < qty_vert + 1; ++i) {
        graph[i].out_vert_list = calloc((size_t)qty_vert+1, sizeof(unsigned int));
        if (!graph[i].out_vert_list) {
            for (int j = 0; j < i; ++j) {
                free(graph[j].out_vert_list);
            }
            free(graph);
            fprintf(stderr, "%s", get_msg(ALLOC_ERR));
            return ALLOC_ERR;
        }
    }

    retcode = add_edges(graph, qty_edg, qty_vert);
    if (retcode) {
        fprintf(stdout, "%s", get_msg(retcode));
        free_list(graph, qty_vert);
        return 0;
    }

    dijkstra(graph, qty_vert, vert_from);
    print_cost_path(graph, qty_vert, vert_from, vert_to);
    free_list(graph, qty_vert);
    return retcode;
}
