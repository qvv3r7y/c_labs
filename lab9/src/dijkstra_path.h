#ifndef LAB9_DIJKSTRA_PATH_H
#define LAB9_DIJKSTRA_PATH_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#define INF ((unsigned) INT_MAX + 1)

#define MAX_ERROR_MSG 7

extern const char *error_msg[MAX_ERROR_MSG];

enum err_code {
    QTY_VERT_ERR = 1,
    QTY_EDGES_ERR,
    IN_VERT_ERR,
    COST_ERR,
    QTY_LINES_ERR,
    ALLOC_ERR
};

typedef struct vertex {
    unsigned int *out_vert_list;
    unsigned int min_cost;
    short parent;
    bool used;
    uint8_t qty_in_path;
} vertex_t;

uint8_t add_edges(vertex_t *graph, int32_t qty_edg, short qty_vert);

void dijkstra(vertex_t *graph, short qty_vert, short vert_from);

void free_list(vertex_t *graph, short qty_vert);

#endif //LAB9_DIJKSTRA_PATH_H
