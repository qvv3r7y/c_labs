#include "dijkstra_path.h"

const char *error_msg[MAX_ERROR_MSG] = {
        "Invalid error code",
        "bad number of vertices",
        "bad number of edges",
        "bad vertex",
        "bad length",
        "bad number of lines",
        "Allocation failure :-("
};


uint8_t add_edges(vertex_t *graph, int32_t qty_edg, short qty_vert) {
    for (int32_t i = 0; i < qty_edg; ++i) {
        short from = 0, to = 0;
        unsigned int cost = 0;
        if (scanf("%hd %hd %u", &from, &to, &cost) == EOF) return QTY_LINES_ERR;
        if (from > qty_vert || to > qty_vert || from < 1 || to < 1) return IN_VERT_ERR;
        if (cost > INT_MAX) return COST_ERR;
        graph[to].out_vert_list[from] = cost;
        graph[from].out_vert_list[to] = cost;
    }
    return 0;
}

static void initialization(vertex_t *graph, short qty_vert, short vert_from) {
    for (int i = 1; i <= qty_vert; ++i) {
        graph[i].min_cost = INF;
        graph[i].parent = -1;
    }
    graph[vert_from].min_cost = 0;
    graph[vert_from].parent = vert_from;
}

static short search_vert(vertex_t *graph, short qty_vert) {
    unsigned int min_cost = INF;
    short vert = 0;
    for (short i = 1; i <= qty_vert; ++i) {
        if (graph[i].used == false && graph[i].min_cost < min_cost) {
            min_cost = graph[i].min_cost;
            vert = i;
        }
    }
    return vert;
}

static void relax_vert(vertex_t *graph, short vert, short qty_vert) {
    for (int i = 1; i <= qty_vert; ++i) {
        if (graph[vert].out_vert_list[i]) {
            if (graph[i].min_cost == INF) graph[i].parent = vert;
            if (graph[vert].min_cost + graph[vert].out_vert_list[i] < graph[i].min_cost) {
                graph[i].min_cost = graph[vert].min_cost + graph[vert].out_vert_list[i];
                graph[i].parent = vert;
            }
            if (graph[i].qty_in_path < 255) graph[i].qty_in_path++;
        }
    }
}

void dijkstra(vertex_t *graph, short qty_vert, short vert_from) {
    initialization(graph, qty_vert, vert_from);
    while (true) {
        short vert = search_vert(graph, qty_vert);
        if (!vert) return;
        graph[vert].used = true;
        relax_vert(graph, vert, qty_vert);
    }
}

void free_list(vertex_t *graph, short qty_vert) {
    for (int i = 0; i <= qty_vert; ++i) {
        free(graph[i].out_vert_list);
    }
    free(graph);
}
