#include <stdio.h>                                                    //    *|     NSU FIT     |*
#include <stdlib.h>                                                   //    *|      LAB_4      |*
#include <string.h>                                                   //    *| by Mateyuk_Ilya |*
                                                                      //    *|     _18212_     |*
#define MAX_LEN_STR 11
#define ASCII_0 47

static const char *set_num = "0123456789";

static void test(const char *arr, int len) {
    int exit1 = 0, exit2 = 0, test_case[10] = {0};
    for (int i = 0; i < len; ++i) {
        if (strchr(set_num, arr[i]) == NULL) exit1 = 1;
    }
    for (int i = 0; i < len; ++i) {
        test_case[arr[i] - ASCII_0]++;
        if (test_case[arr[i] - ASCII_0] == 2) exit2 = 1;
    }
    if (exit1 || exit2) {
        printf("bad input");
        exit(EXIT_SUCCESS);
    }
}

static void swap(char *arr, int i, int j) {
    char temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}

static void print_permutation(char *arr, int qty_perm, int len) {
    for (int count = 0; count < qty_perm; ++count) {
        int i = len - 2, j = len - 1;
        while ((i >= 0) && (arr[i] >= arr[i + 1])) {
            --i;
        }
        if (i == -1) exit(EXIT_SUCCESS);
        while (arr[i] >= arr[j]) --j;
        swap(arr, i, j);
        int left = i + 1, right = len - 1;
        while (left < right) swap(arr, left++, right--);
        printf("%s\n", arr);
    }
}

int main() {
    char arr[MAX_LEN_STR] = {0};
    int qty_perm;
    scanf("%10s", arr);
    scanf("%d", &qty_perm);
    int len = strlen(arr);
    test(arr, len);
    print_permutation(arr, qty_perm, len);
}
