#include <stdio.h>                                               //    *|     NSU FIT     |*
#include <string.h>                                              //    *|      LAB_3      |*
#include <stdlib.h>                                              //    *| by Mateyuk_Ilya |*
#include <math.h>                                                //    *|     _18212_     |*

#define MAX_LEN_PATT 16
#define REHASH(hash, add_el, pow_3) ((hash) / 3 + ((add_el) % 3) * pow_3)
typedef unsigned long ulong_t;

static ulong_t get_hash(const unsigned char *arr, int len) {
    ulong_t hash = 0, pow_3 = 1;
    for (int i = 0; i < len; ++i) {
        hash += arr[i] % 3 * pow_3;
        pow_3 *= 3;
    }
    return hash;
}

static void add_el_buf(unsigned char *buf, int len, ulong_t *hash_buf, ulong_t pow_3, const ulong_t *last) {
    int symb = getchar();
    if (symb == EOF) exit(EXIT_SUCCESS);
    *hash_buf = REHASH(*hash_buf, symb, pow_3);
    buf[*last % len] = (unsigned char) symb;
}

static void write_buf(unsigned char *buf, int len) {
    for (int i = 0; i < len; ++i) {
        int symb = getchar();
        if (symb == EOF) exit(EXIT_SUCCESS);
        buf[i] = (unsigned char) symb;
    }
}

static void rabin_karp_search(const unsigned char *pattern, int len) {
    unsigned char buf[MAX_LEN_PATT + 1] = {0};
    ulong_t hash_patt = 0, hash_buf = 0, pow_3 = 1, last = (ulong_t) len;
    for (int j = 0; j < len - 1; ++j) {
        pow_3 = pow_3 * 3;
    }
    hash_patt = get_hash(pattern, len);
    printf("%lu ", hash_patt);
    write_buf(buf, len);
    hash_buf = get_hash(buf, len);
    while (1) {
        if (hash_buf == hash_patt) {
            for (int i = 0; i < len; ++i) {
                printf("%lu ", last - len + i + 1);
                if (buf[(last - len + i) % len] != pattern[i]) break;
            }
        }
        add_el_buf(buf, len, &hash_buf, pow_3, &last);
        last++;
    }
}

int main() {
    unsigned char pattern[MAX_LEN_PATT + 1] = {0};
    scanf("%16[^\n]s", pattern);
    getchar();                                                    //Read unnecessary '\n'
    size_t len = strlen((const char *) pattern);
    rabin_karp_search(pattern, len);
    return 0;
}
