#include <stdio.h>                                               //    *|     NSU FIT     |*
#include <string.h>                                              //    *|      LAB_1      |*
#include <stdlib.h>                                              //    *| by Mateyuk_Ilya |*
#include <limits.h>                                              //    *|     _18212_     |*

#define MAX_LEN_PATT 16

struct circular_buffer {
    int buf[MAX_LEN_PATT];
    long long int last;
    size_t len;
};

static void make_set(int *set, unsigned const char *str, size_t len) {
    for (int i = 0; i <= UCHAR_MAX; ++i) {
        set[i] = len;
    }
    for (int i = 0; i < len - 1; ++i) {
        set[str[i]] = len - 1 - i;
    }
}

static void push_circular_buf(struct circular_buffer *cir_buf, size_t shift) {
    for (int i = 0; i < shift; ++i) {
        int symb = getchar();
        if (symb != EOF) cir_buf->buf[(cir_buf->last + i) % cir_buf->len] = symb;
        else exit(EXIT_SUCCESS);
    }
    cir_buf->last += shift;
}

static void boyer_moore(const int *shift_set, unsigned const char *patt, struct circular_buffer *cir_buf) {
    size_t shift = cir_buf->len;
    while (1) {
        push_circular_buf(cir_buf, shift);
        shift = (size_t) shift_set[cir_buf->buf[(cir_buf->last - 1) % cir_buf->len]];
        for (int i = cir_buf->len - 1; i >= 0; --i) {
            printf("%lli ", cir_buf->last - cir_buf->len + 1 + i);
            if (patt[i] != cir_buf->buf[(cir_buf->last + i) % cir_buf->len]) break;
            else if (!i) shift = cir_buf->len;
        }
    }
}

int main() {
    int shift_set[UCHAR_MAX + 1];
    unsigned char pattern[MAX_LEN_PATT + 1] = {0};
    struct circular_buffer cir_buf = {0};
    scanf("%16[^'\n']s", pattern);
    getchar();                                                   //Read unnecessary '\n' symbol
    cir_buf.len = strlen((const char *) pattern);
    make_set(shift_set, pattern, cir_buf.len);
    boyer_moore(shift_set, pattern, &cir_buf);
    return 0;
}
