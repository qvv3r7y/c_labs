
#include "AVL_tree.h"

int main() {
    int n = 0;
    node *root = NULL;
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        int key = 0;
        scanf("%d", &key);
        root = insert(root, key);
        if(!root) return ALLOCATION_FAILURE;
    }
    printf("%d", height(root));
    delete_tree(root);
    return 0;
}
