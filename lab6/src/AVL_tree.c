
#include "AVL_tree.h"

static jmp_buf env;

void delete_tree(node *root) {
    if (root) {
        delete_tree(root->right);
        delete_tree(root->left);
        free(root);
    }
}


static inline int balance_factor(node *tmp) {
    return (int) height(tmp->right) - height(tmp->left);
}

static void correct_height(node *tmp) {
    uint8_t r_height = height(tmp->right);
    uint8_t l_height = height(tmp->left);
    tmp->height = (r_height > l_height ? r_height : l_height) + (uint8_t) 1;
}

inline uint8_t height(node *tmp) {
    return tmp ? tmp->height : 0;
}

typedef node **(*l_r_turn)(node *tmp);

static node **left(node *tmp) {
    return &tmp->left;
}

static node **right(node *tmp) {
    return &tmp->right;
}

static node *_turn(node *root, l_r_turn from, l_r_turn to) {
    node *tmp = *from(root);
    *from(root) = *to(tmp);
    *to(tmp) = root;
    correct_height(root);
    correct_height(tmp);
    return tmp;
}

static node *turn(node *root, l_r_turn rotate) {
    if (rotate == left) return _turn(root, right, left);
    if (rotate == right) return _turn(root, left, right);
    return NULL;
}

static node *balance_avl(node *root) {
    correct_height(root);
    if (balance_factor(root) == 2) {
        if (balance_factor(root->right) < 0) root->right = turn(root->right, right);
        return turn(root, left);
    }
    if (balance_factor(root) == -2) {
        if (balance_factor(root->left) > 0) root->left = turn(root->left, left);
        return turn(root, right);
    }
    return root;
}

static node *new_node(int key) {
    node *new = malloc(sizeof(node));
    if (!new) {
        longjmp(env, ALLOCATION_FAILURE);
    }
    new->left = new->right = NULL;
    new->height = 1;
    new->key = key;
    return new;
}

static node *_insert(node *root, int key) {
    if (!root) return new_node(key);
    if (key < root->key) root->left = _insert(root->left, key);
    else root->right = _insert(root->right, key);
    return balance_avl(root);
}

node *insert(node *root, int key) {
    if (setjmp(env) != 0) {
        fprintf(stderr, "Error code '%d:' \nAllocation failure", ALLOCATION_FAILURE);
        delete_tree(root);
        return NULL;
    }
    return _insert(root, key);
}
