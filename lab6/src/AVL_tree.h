
#ifndef BALANCED_TREES_AVL_TREE_H
#define BALANCED_TREES_AVL_TREE_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <setjmp.h>

#define ALLOCATION_FAILURE 1

typedef struct node {
    int key;
    struct node *left;
    struct node *right;
    uint8_t height;
} node;

uint8_t height(node *tmp);

node *insert(node *root, int key);

void delete_tree(node *root);

#endif //BALANCED_TREES_AVL_TREE_H
