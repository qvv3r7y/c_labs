#include "prim.h"

#define MAX_QTY_VERT 5000

static uint8_t check_graph(const vertex_t *graph, short qty_vert) {
    for (int i = 1; i <= qty_vert; ++i) {
        if (graph[i].parent == -1) {
            fprintf(stdout, "%s", get_err_msg(BAD_GRAPH_ERR));
            return BAD_GRAPH_ERR;
        }
    }
    return 0;
}

static void print_mst(const vertex_t *graph, short qty_vert) {
    for (int i = 1; i <= qty_vert; ++i) {
        if (graph[i].parent != i) printf("%hd %d\n", graph[i].parent, i);
    }
}

int main(void) {
    uint8_t retcode = 0;
    short qty_vert = 0;
    int32_t qty_edge = 0;
    if (scanf("%hd", &qty_vert) != 1) retcode = QTY_LINES_ERR;
    if (scanf("%d", &qty_edge) != 1) retcode = QTY_LINES_ERR;
    if (qty_vert > MAX_QTY_VERT) retcode = QTY_VERT_ERR;
    if (qty_edge > (int32_t) (qty_vert * (qty_vert + 1) / 2)) retcode = QTY_EDGE_ERR;
    if (qty_vert == 0) retcode = BAD_GRAPH_ERR;
    if (retcode) {
        fprintf(stdout, "%s", get_err_msg(retcode));
        return 0;
    }

    vertex_t *graph = calloc((size_t) qty_vert + 1, sizeof(vertex_t));
    if (!graph) {
        fprintf(stderr, "%s", get_err_msg(ALLOC_ERR));
        return ALLOC_ERR;
    }

    for (int i = 0; i < qty_vert + 1; ++i) {
        graph[i].out_vert_list = calloc((size_t) qty_vert + 1, sizeof(unsigned int));
        if (!graph[i].out_vert_list) {
            for (int j = 0; j < i; ++j) {
                free(graph[j].out_vert_list);
            }
            free(graph);
            fprintf(stderr, "%s", get_err_msg(ALLOC_ERR));
            return ALLOC_ERR;
        }
    }

    retcode = add_edges(graph, qty_edge, qty_vert);
    if (retcode) {
        fprintf(stdout, "%s", get_err_msg(retcode));
        free_list(graph, qty_vert);
        return 0;
    }

    prim(graph, qty_vert);
    if (check_graph(graph, qty_vert)) {
        free_list(graph, qty_vert);
        return 0;
    }

    print_mst(graph, qty_vert);
    free_list(graph, qty_vert);
    return retcode;
}
