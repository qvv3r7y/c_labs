#include "prim.h"

const char *get_err_msg(enum err_code err){
    switch (err){
        case QTY_VERT_ERR:
            return "bad number of vertices";
        case QTY_EDGE_ERR:
            return "bad number of edges";
        case IN_VERT_ERR:
            return "bad vertex";
        case COST_ERR:
            return "bad length";
        case QTY_LINES_ERR:
            return "bad number of lines";
        case BAD_GRAPH_ERR:
            return "no spanning tree";
        case ALLOC_ERR:
            return "Allocation failure";
    }
    return "invalid_err_code";
}

uint8_t add_edges(vertex_t *graph, int32_t qty_edg, short qty_vert) {
    for (int32_t i = 0; i < qty_edg; ++i) {
        short from = 0, to = 0;
        unsigned int cost = 0;
        if (scanf("%hd %hd %u", &from, &to, &cost) != 3) return QTY_LINES_ERR;
        if (from > qty_vert || to > qty_vert || from < 1 || to < 1) return IN_VERT_ERR;
        if (cost > INT_MAX) return COST_ERR;
        graph[to].out_vert_list[from] = cost;
        graph[from].out_vert_list[to] = cost;
    }
    return 0;
}

static void initialization(vertex_t *graph, short qty_vert) {
    for (int i = 2; i <= qty_vert; ++i) {
        graph[i].min_edge = INF;
        graph[i].parent = -1;
    }
    graph[1].parent = 1;
}

static short search_vert(const vertex_t *graph, short qty_vert) {
    unsigned int min_edge = INF;
    short vert = 0;
    for (short i = 1; i <= qty_vert; ++i) {
        if (!graph[i].used && graph[i].min_edge < min_edge) {
            min_edge = graph[i].min_edge;
            vert = i;
        }
    }
    return vert;
}

static void relax_vert(vertex_t *graph, short vert, short qty_vert) {
    for (int i = 1; i <= qty_vert; ++i) {
        if (graph[i].used || graph[vert].out_vert_list[i] == 0) continue;
        if (graph[vert].out_vert_list[i] < graph[i].min_edge) {
            graph[i].min_edge = graph[vert].out_vert_list[i];
            graph[i].parent = vert;
        }
    }
}

void prim(vertex_t *graph, short qty_vert) {
    initialization(graph, qty_vert);
    while (true) {
        short vert = search_vert(graph, qty_vert);
        if (!vert) {
            return;
        }
        graph[vert].used = true;
        relax_vert(graph, vert, qty_vert);
    }
}

void free_list(vertex_t *graph, short qty_vert) {
    for (int i = 0; i <= qty_vert; ++i) {
        free(graph[i].out_vert_list);
    }
    free(graph);
}
