
#ifndef LAB8_1_PRIM_H
#define LAB8_1_PRIM_H

#include <stdbool.h>
#include <stdint.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

#define INF ((unsigned) INT_MAX + 1)

enum err_code {
    QTY_VERT_ERR = 1,
    QTY_EDGE_ERR,
    IN_VERT_ERR,
    COST_ERR,
    QTY_LINES_ERR,
    BAD_GRAPH_ERR,
    ALLOC_ERR
};

const char *get_err_msg(enum err_code err);

typedef struct vertex {
    unsigned int *out_vert_list;
    unsigned int min_edge;
    short parent;
    bool used;
} vertex_t;

uint8_t add_edges(vertex_t *graph, int32_t qty_edg, short qty_vert);

void prim(vertex_t *graph, short qty_vert);

void free_list(vertex_t *graph, short qty_vert);

#endif //LAB8_1_PRIM_H
