
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include "reverse_polish.h"

#define BASE 10

static operator_code which_operator(int symb) {
    switch (symb) {
        case '+':
            return ADD;
        case '-':
            return SUB;
        case '*':
            return MUL;
        case '/':
            return DIV;
        default:
            return (operator_code) 0;
    }
}

static int priority(operator_code symb) {
    switch (symb) {
        case ADD:
        case SUB:
            return 1;
        case MUL:
        case DIV:
            return 2;
    }
	return 0;
}

static void put_digit(int *symb, int *polish_arr, int *index_arr, int *p_empty_brackets) {
    char str[MAX_LEN_STR];
    memset(str, '#', MAX_LEN_STR);                                          // # - stop symb for strtol()
    for (int i = 0; isdigit(*symb); ++i) {
        str[i] = (char) *symb;
        *symb = getchar();
    }
    polish_arr[*index_arr] = (int) strtol(str, NULL, BASE);
    (*index_arr)++;
    *p_empty_brackets = 0;
}

static void put_operator(stack **head, int *symb, int *polish_arr, int *index_arr) {
    operator_code push_operator = which_operator(*symb);
    while (*head != NULL) {
        int top_operator = view_head(head);
        if (top_operator == '(') break;
        if (priority(push_operator) > priority(top_operator)) break;
        polish_arr[*index_arr] = pop(head);
        (*index_arr)++;
    }
    push(push_operator, head);
    *symb = getchar();
}

static void put_bracket_open(stack **head, int *symb, int *p_balance_bracket, int *p_empty_brackets) {
    push('(', head);
    *symb = getchar();
    (*p_balance_bracket)++;
    if (*symb != '(')*p_empty_brackets = 1;
}

static void put_bracket_close(stack **head, int *symb, int *polish_arr, int *index_arr, int *p_balance_bracket) {
    int top_operator = pop(head);
    while (top_operator != '(') {
        polish_arr[*index_arr] = top_operator;
        (*index_arr)++;
        top_operator = pop(head);
    }
    *symb = getchar();
    (*p_balance_bracket)--;
}

static void test_syntax(int balance_bracket, int index_arr, int empty_brackets) {
    short exit_1 = 0, exit_2 = 0, exit_3 = 0;
    if (balance_bracket != 0) exit_1 = 1;
    if (index_arr == 0) exit_2 = 1;
    if (empty_brackets == 1) exit_3 = 1;
    if (exit_1 || exit_2 || exit_3) {
        printf("syntax error");
        exit(EXIT_SUCCESS);
    }
}

int reverse_polish(stack **head, int *polish_arr) {
    const char *valid_char = "0123456789+-*/()\n";
    int index_arr = 0, symb = getchar(), balance_bracket = 0, empty_brackets = 0;
    int *p_symb = &symb, *p_index_arr = &index_arr, *p_balance_bracket = &balance_bracket, *p_empty_brackets = &empty_brackets;
    while (*p_symb != '\n') {
        if (*p_symb == '(') put_bracket_open(head, p_symb, p_balance_bracket, p_empty_brackets);
        if (isdigit(*p_symb)) put_digit(p_symb, polish_arr, p_index_arr, p_empty_brackets);
        else if (*p_symb == ')') put_bracket_close(head, p_symb, polish_arr, p_index_arr, p_balance_bracket);
        else if (which_operator(*p_symb)) put_operator(head, p_symb, polish_arr, p_index_arr);
        else if (!strchr(valid_char, symb)) {
            printf("syntax error");
            exit(EXIT_SUCCESS);
        }
        if (empty_brackets == 1) break;
    }
    test_syntax(balance_bracket, index_arr, empty_brackets);
    while (*head != NULL) {
        polish_arr[index_arr] = pop(head);                                       //pop last operators
        index_arr++;
    }
    return index_arr;
}

