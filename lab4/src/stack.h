
#ifndef CALC_STACK_H
#define CALC_STACK_H

typedef struct stack {
    int element;
    struct stack *prev;
} stack;

stack **create_stack(void);

void push(int data, stack **head);

int pop(stack **head);

int view_head(stack **head);

void delete_stack(stack **head);

#endif //CALC_STACK_H
