
#ifndef CALC_REVERSE_POLISH_H
#define CALC_REVERSE_POLISH_H

#define MAX_LEN_STR 1000

typedef enum operator_code {
    ADD = -1,
    SUB = -2,
    MUL = -3,
    DIV = -4
} operator_code;

int reverse_polish(stack **head, int *polish_arr);

#endif //CALC_REVERSE_POLISH_H
