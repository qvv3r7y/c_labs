#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include "reverse_polish.h"
#include "calc.h"

static int get_result_operation(int a, int b, int operator) {
    switch (operator) {
        case ADD:
            return a + b;
        case SUB:
            return a - b;
        case MUL:
            return a * b;
        case DIV:
            if (b == 0) {
                printf("division by zero");
                exit(0);
            }
            return a / b;
        default: exit(1);
    }
}

void calc(const int *polish_arr, int qty_symb, stack **head) {
    if (qty_symb == 1) {
        printf("%d", polish_arr[0]);
        exit(0);
    }
    for (int i = 0; i < qty_symb; ++i) {
        if (polish_arr[i] >= 0) {
            push(polish_arr[i], head);
        }
        else push(get_result_operation(pop(head), pop(head), polish_arr[i]), head);
    }
    printf("%d", pop(head));
}

