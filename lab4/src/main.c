
#include "stack.h"
#include "calc.h"
#include "reverse_polish.h"

int main() {
    int polish_arr[MAX_LEN_STR] = {0};
    int qty_symb = 0;
    stack **head = create_stack();
    qty_symb = reverse_polish(head, polish_arr);
    calc(polish_arr, qty_symb, head);
    delete_stack(head);
    return 0;
}

