
#include <stdlib.h>
#include <stdio.h>
#include "stack.h"

static void check_allocation(void *p) {
    if (p == NULL) {
        perror("Allocation failure");
        exit(EXIT_FAILURE);
    }
}

stack **create_stack(void) {
    stack **head = malloc(sizeof(stack *));
    check_allocation(head);
    *head = NULL;
    return head;
}

void push(int data, stack **head) {
    stack *last = malloc(sizeof(stack));
    check_allocation(last);
    last->prev = *head;
    last->element = data;
    *head = last;
}

int pop(stack **head) {
    if (*head == NULL) {
        printf("syntax error");
        exit(0);
    }
    int symbol = (*head)->element;
    stack *tmp = (*head)->prev;
    free(*head);
    *head = tmp;
    return symbol;
}

int view_head(stack **head) {
    return (*head)->element;
}

void delete_stack(stack **head) {
    while (*head) {
        stack *tmp = (*head)->prev;
        free(*head);
        *head = tmp;
    }
    free(head);
}

