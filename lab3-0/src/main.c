#include <stdio.h>                                                      //    *|     NSU FIT     |*
#include <stdlib.h>                                                     //    *|      LAB_5      |*
                                                                        //    *| by Mateyuk_Ilya |*
																	    //    *|     _18212_     |*
static void allocation_arr(int **arr, int size_arr) {
    *arr = calloc((size_t) size_arr, sizeof(**arr));
    if (*arr == NULL) {
        perror("Allocation failure");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < size_arr; ++i) {
        scanf("%d", *arr + i);
    }
}

static void swap(int *x, int *y) {
    int temp = *x;
    *x = *y;
    *y = temp;
}

static void mk_binary_tree(int *arr, int size, int sort_element) {
    for (int i = sort_element; i >= 0; --i) {
        sort_element = i;
        while (sort_element * 2 + 1 < size) {
            int child_1 = 2 * sort_element + 1;
            int child_2 = 2 * sort_element + 2;
            int max = child_1;
            if (child_2 != size) max += (arr[child_1] > arr[child_2] ? 0 : 1);
            if (arr[sort_element] < arr[max]) {
                swap(&arr[sort_element], &arr[max]);
                sort_element = max;
            } else break;
        }
    }
}

static void heap_sort(int *arr, int size) {
    while (size > 1) {
        mk_binary_tree(arr, size, 0);
        swap(&arr[0], &arr[size - 1]);
        size--;
    }
}

static void print_arr(const int *arr, int size) {
    for (int i = 0; i < size; ++i) {
        printf("%d ", arr[i]);
    }
}

int main() {
    int size_arr;
    int *arr;
    scanf("%d", &size_arr);
    allocation_arr(&arr, size_arr);
    mk_binary_tree(arr, size_arr, size_arr / 2 - 1);
    heap_sort(arr, size_arr);
    print_arr(arr, size_arr);
    free(arr);
    return 0;
}
