
#ifndef LAB7_TOP_SORT_H
#define LAB7_TOP_SORT_H

#include <stdio.h>
#include <stdlib.h>

#define MAX_VERTEX 1000

#define GREY 1
#define BLACK 2

enum error {
    QTY_VERT_ERR = 1,
    QTY_EDGES_ERR,
    IN_VERT_ERR,
    QTY_LINES_ERR,
    CYCLE_ERR,
    ALLOC_ERROR
};

#define MAX_ERROR_MSG 7

extern const char *error_msg[MAX_ERROR_MSG];

typedef struct node {
    int vert;
    struct node *prev;
} node;

typedef struct vertex {
    node *out_vert;
    char color;
} vertex;

int add_edges(int qty_edges, vertex *adj_list, int qty_vert);

int top_sort(int qty_vert, vertex *adj_list);

#endif //LAB7_TOP_SORT_H
