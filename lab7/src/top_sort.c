
#include "top_sort.h"

const char *error_msg[MAX_ERROR_MSG] = {
        "invalid msg code",
        "bad number of vertices",
        "bad number of edges",
        "bad vertex",
        "bad number of lines",
        "impossible to sort",
        "Allocation failure"
};


static int push_vert(int vert, node **head) {
    node *new = malloc(sizeof(node));
    if (!new) return ALLOC_ERROR;
    new->prev = *head;
    new->vert = vert;
    *head = new;
    return 0;
}

static int pop_vert(node **head) {
    int vert = (*head)->vert;
    node *tmp = (*head)->prev;
    free(*head);
    *head = tmp;
    return vert;
}

int add_edges(int qty_edges, vertex *adj_list, int qty_vert) {
    for (int i = 0; i < qty_edges; ++i) {
        int a = 0, b = 0, retcode = 0;
        if (scanf("%d %d", &a, &b) == EOF) return QTY_LINES_ERR;
        if (a <= 0 || b <= 0 || a > qty_vert || b > qty_vert) return IN_VERT_ERR;
        retcode = push_vert(b, &(adj_list[a].out_vert));
        if (retcode) return retcode;
    }
    return 0;
}

static int dfs(int vert, vertex *adj_list, node **sort_list) {
    if (adj_list[vert].color == GREY) {
        return CYCLE_ERR;
    }
    if (adj_list[vert].color != BLACK) {
        int retcode = 0;
        adj_list[vert].color = GREY;
        while (adj_list[vert].out_vert) {
            retcode = dfs(pop_vert(&adj_list[vert].out_vert), adj_list, sort_list);
            if (retcode) return retcode;
        }
        adj_list[vert].color = BLACK;
        retcode = push_vert(vert, sort_list);
        if (retcode) return retcode;
    }
    return 0;
}

static void print_sort_list(node *sort_list, int retcode) {
    while (sort_list) {
        int vert = pop_vert(&sort_list);
        if (!retcode) printf("%d ", vert);
    }
}

int top_sort(int qty_vert, vertex *adj_list) {
    int retcode = 0;
    node *sort_list = NULL;
    for (int i = 1; i <= qty_vert; ++i) {
        retcode = dfs(i, adj_list, &sort_list);
        if (retcode) break;
    }
    print_sort_list(sort_list, retcode);
    return retcode;
}
