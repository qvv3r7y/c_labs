
#include "top_sort.h"

static inline const char *ts_msg(int code) {
    return (code < MAX_ERROR_MSG) ? error_msg[code] : error_msg[0];
}

int main(void) {
    int retcode = 0;
    int qty_vert = 0, qty_edges = 0;

    if (scanf("%d", &qty_vert) == EOF) retcode = QTY_LINES_ERR;
    if (scanf("%d", &qty_edges) == EOF) retcode = QTY_LINES_ERR;
    if (qty_vert > MAX_VERTEX) retcode = QTY_VERT_ERR;
    if (qty_edges > qty_vert * (qty_vert + 1) / 2) retcode = QTY_EDGES_ERR;
    if (retcode) {
        fprintf(stdout, "%s", ts_msg(retcode));
        return 0;//retcode;
    }

    vertex *adj_list = calloc((size_t) (qty_vert + 1), sizeof(vertex));
    if (!adj_list) {
        fprintf(stdout, "%s", ts_msg(ALLOC_ERROR));
        return 0;// retcode;
    }

    retcode = add_edges(qty_edges, adj_list, qty_vert);
    if (retcode) fprintf(stdout, "%s", ts_msg(retcode));
    retcode = top_sort(qty_vert, adj_list);
    if (retcode) fprintf(stdout, "%s", ts_msg(retcode));

    free(adj_list);
    return 0;//retcode;
}
