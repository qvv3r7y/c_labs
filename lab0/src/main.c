#include <stdio.h>                        //    *|     NSU FIT     |*
#include <string.h>                       //    *|      LAB_0      |*
#include <math.h>                         //    *| by Mateyuk_Ilya |*
#include <stdlib.h>                       //    *|     _18212_     |*
#include <ctype.h>
#include <stdint.h>

static const char SSS[] = "0123456789ABCDEF";

static void test(int point, size_t len, int a, int b, char *str) {
    for (int i = 0; i < len; ++i) {
        int exit0 = 0, exit1 = 0, exit2 = 0, exit3 = 0, exit4 = 0;
        str[i] = (char) (toupper(str[i]));
        char *p_symb_SSS = strchr(SSS, str[i]);
        exit0 = ((a < 2) || (b < 2) || (a > 16) || (b > 16));
        exit1 = (str[0] == '.') || (str[len - 1] == '.');
        exit2 = (p_symb_SSS == NULL) && (i != point);          // Недопустимый символ
        if (p_symb_SSS != NULL)
            exit3 = (p_symb_SSS - SSS) >= a;                       // Если цифра >= основанию
        exit4 = (str[0] == '0') && (str[1] != '.') && (len > 1);     // Есть лишний ноль в начале
        if (exit0 || exit1 || exit2 || exit3 || exit4) {
            printf("bad input");
            exit(EXIT_SUCCESS);
        }
    }
}

static double aTo10(int point, size_t len, int a, const char *str) {
    double res = 0;
    int z;
    if (point == 0)
        point = len;
    for (int i = 0; i < point; ++i) {                               // Считаем целую часть
        z = (int) (strchr(SSS, str[i]) - SSS);                      // Берем очередное число из строки
        res += z * pow(a, point - i - 1);
    }
    for (int i = point + 1; i < len; ++i) {                      // Считаем дробную часть, если есть точка
        z = (int) (strchr(SSS, str[i]) - SSS);
        res += z * pow(a, -(i - point));
    }
    return res;
}

static void reverse_b_int(char *str) {
    size_t k = strlen(str);
    for (int i = 0; i < k / 2; ++i) {
        char symb = str[i];
        str[i] = str[k - i - 1];
        str[k - i - 1] = symb;
    }
}

static void dec2b_int(uintmax_t dec, int b, char *str_b_int) {                   // Целая часть из 10 в b-ричную
    if (dec == 0) str_b_int[0] = '0';
    for (int i = 0; dec > 0; ++i) {
        char symb = SSS[dec % b];
        dec = dec / b;
        str_b_int[i] = symb;
    }
    reverse_b_int(str_b_int);
}

static void dec2b_frac(double dec, int b, char *s) {                       // Перевод дробной части в b-ричную
    double p = 0;
    for (int i = 0; i < 12; ++i) {                                  // Находим 12 символов после точки
        p = dec * b;
        char symb = SSS[(uintmax_t) p];
        dec = p - (uintmax_t) p;
        *s = symb;
        ++s;
    }
}

static void cls_0(char *str_b_frac) {
    for (int i = 12; (i >= 2) && (str_b_frac[i] == '0'); --i) {
        str_b_frac[i] = '\0';                                       // Убирает лишние нули
    }
}

int main() {
    int a, b;
    double result_10;
    char str_in[14] = {0}, str_b_int[50] = {0}, str_b_frac[14] = {'.', 0};
    scanf("%d%d", &a, &b);
    scanf("%13s", str_in);
    size_t len = strlen(str_in);
    int point;
    if (strchr(str_in, '.') == NULL)
        point = 0;
    else
        point = (int) (strchr(str_in, '.') - str_in);               // Номер точки в строке
    test(point, len, a, b, str_in);
    result_10 = aTo10(point, len, a, str_in);                    // Число в 10
    dec2b_int((uintmax_t) result_10, b, str_b_int);                     // Целое в b-ричной
    if (point != 0) {
        result_10 = result_10 - (uintmax_t) result_10;
        dec2b_frac(result_10, b, str_b_frac + 1);                   // Дробная часть в b-ричной
        cls_0(str_b_frac);                                          // Убираем лишние нолики
        strcat(str_b_int, str_b_frac);
    }
    printf("%s", str_b_int);
    return 0;
}
