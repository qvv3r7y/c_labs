#include <stdio.h>                                                    //    *|     NSU FIT     |*
#include <stdlib.h>                                                   //    *|      LAB_6      |*
                                                                      //    *| by Mateyuk_Ilya |*
                                                                      //    *|     _18212_     |*
static void get_alloc_arr(int **arr, int size_arr) {
    *arr = calloc((size_t) size_arr, sizeof(int));
    if (*arr == NULL) {
        perror("Allocation failed");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < size_arr; ++i) {                         /// arr[i] = *(arr + i) - указ
        scanf("%d", &(*arr)[i]);                                 /// (*arr)[i] = *(*arr + i) - указ на указ
    }
}

static void print_arr(const int *arr, int size_arr) {
    for (int i = 0; i < size_arr; ++i) {
        printf("%d ", arr[i]);
    }
}

static void quick_sort(int *arr, int size_arr) {
    int left = 0, right = size_arr - 1, mid_element = arr[size_arr / 2];
    do {
        while (arr[left] < mid_element) ++left;
        while (arr[right] > mid_element) --right;
        if (left <= right) {
            int x = arr[left];
            arr[left] = arr[right];
            arr[right] = x;
            left++;
            right--;
        }
    } while (left <= right);
    if (right > 0) quick_sort(arr, right + 1);
    if (left < size_arr) quick_sort(arr + left, size_arr - left);
}

int main() {
    int size_arr;
    int *arr;
    scanf("%d", &size_arr);
    get_alloc_arr(&arr, size_arr);
    quick_sort(arr, size_arr);
    print_arr(arr, size_arr);
    free(arr);
}
