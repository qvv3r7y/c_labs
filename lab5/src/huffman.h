#ifndef LAB5_HUFFMAN_H
#define LAB5_HUFFMAN_H

#include <stdio.h>
#include <stdlib.h>

#define END_OF_STREAM    256
#define ESCAPE           257
#define QTY_SYMBOL       (256 + 2)
#define QTY_NODE         ((QTY_SYMBOL * 2) - 1)
#define ROOT_NODE        0

enum in_out_flag{
    OUT_STREAM = 0,
    IN_STREAM = 1
};

typedef struct compressed_file {
    FILE *file;
    unsigned char mask;
    int block;
} compressed_file_t;

typedef struct tree {
    int leaf[QTY_SYMBOL];
    int next_free_node;
    struct node {
        unsigned int weight;
        int parent;
        int child_is_leaf;
        int child;
    } nodes[QTY_NODE];
} tree_t;

void compress_file(FILE *input, compressed_file_t *output);

void expand_file(compressed_file_t *input, FILE *output);

void close_bit_in_out(compressed_file_t *file, enum in_out_flag in_out_flag);

compressed_file_t *open_for_bit_in_out(const char *stream, enum in_out_flag in_out_flag);

#endif //LAB5_HUFFMAN_H
