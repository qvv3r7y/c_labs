#include "huffman.h"
#include <stdbool.h>

compressed_file_t *open_for_bit_in_out(const char *stream, enum in_out_flag in_out_flag) {
    compressed_file_t *file = calloc(1, sizeof(compressed_file_t));
    if (!file) {
        perror("Allocation in open_for_bit_in_out failed");
        return NULL;
    }
    file->block = 0;
    file->mask = 1 << 7;
    if (in_out_flag == IN_STREAM) file->file = fopen(stream, "rb");
    else file->file = fopen(stream, "wb");
    return (file);
}


void close_bit_in_out(compressed_file_t *file, enum in_out_flag in_out_flag) {
    if ((in_out_flag == OUT_STREAM) && (file->mask != 128)) {
        if (putc(file->block, file->file) != file->block) printf("Error for decode");
    }
    fclose(file->file);
    free(file);
}

static int out_bits(compressed_file_t *out_file, unsigned long int code, int count) {
    unsigned long mask = 1 << (count - 1);
    while (mask != 0) {
        if (mask & code) out_file->block |= out_file->mask;
        out_file->mask >>= 1;
        if (out_file->mask == 0) {
            if (putc(out_file->block, out_file->file) != out_file->block) {
                fprintf(stderr, "Error in out_bits");
                return 1;
            }
            out_file->block = 0;
            out_file->mask = 1 << 7;
        }
        mask >>= 1;
    }
    return 0;
}

static unsigned long in_bits(compressed_file_t *in_file, int count) {
    unsigned long value = 0;
    unsigned long mask = 1 << (count - 1);
    while (mask != 0) {
        if (in_file->mask == 1 << 7) {
            in_file->block = getc(in_file->file);
            if (in_file->block == EOF) fprintf(stderr, "Error in in_bits");
        }
        if (in_file->block & in_file->mask) value |= mask;
        mask >>= 1;
        in_file->mask >>= 1;
        if (in_file->mask == 0) in_file->mask = 1 << 7;
    }
    return value;
}


static void initialize_tree(tree_t *tree) {
    for (int i = 0; i < END_OF_STREAM; ++i) {
        tree->leaf[i] = -1;
    }
    tree->next_free_node = ROOT_NODE + 3;

    tree->nodes[ROOT_NODE].child = ROOT_NODE + 1;
    tree->nodes[ROOT_NODE].child_is_leaf = false;
    tree->nodes[ROOT_NODE].weight = 2;
    tree->nodes[ROOT_NODE].parent = -1;

    tree->nodes[ROOT_NODE + 1].child = END_OF_STREAM;
    tree->nodes[ROOT_NODE + 1].child_is_leaf = true;
    tree->nodes[ROOT_NODE + 1].weight = 1;
    tree->nodes[ROOT_NODE + 1].parent = ROOT_NODE;
    tree->leaf[END_OF_STREAM] = ROOT_NODE + 1;

    tree->nodes[ROOT_NODE + 2].child = ESCAPE;
    tree->nodes[ROOT_NODE + 2].child_is_leaf = true;
    tree->nodes[ROOT_NODE + 2].weight = 1;
    tree->nodes[ROOT_NODE + 2].parent = ROOT_NODE;
    tree->leaf[ESCAPE] = ROOT_NODE + 2;
}


static void swap_nodes(tree_t *tree, int i, int j) {
    struct node tmp;

    if (tree->nodes[i].child_is_leaf)
        tree->leaf[tree->nodes[i].child] = j;
    else {
        tree->nodes[tree->nodes[i].child].parent = j;
        tree->nodes[tree->nodes[i].child + 1].parent = j;
    }
    if (tree->nodes[j].child_is_leaf)
        tree->leaf[tree->nodes[j].child] = i;
    else {
        tree->nodes[tree->nodes[j].child].parent = i;
        tree->nodes[tree->nodes[j].child + 1].parent = i;
    }
    tmp = tree->nodes[i];
    tree->nodes[i] = tree->nodes[j];
    tree->nodes[i].parent = tmp.parent;
    tmp.parent = tree->nodes[j].parent;
    tree->nodes[j] = tmp;
}


static void update_tree(tree_t *tree, int c) {
    int current_node;
    int new_node;

    current_node = tree->leaf[c];
    while (current_node != -1) {
        tree->nodes[current_node].weight++;
        for (new_node = current_node; new_node > ROOT_NODE; new_node--)
            if (tree->nodes[new_node - 1].weight >=
                tree->nodes[current_node].weight)
                break;
        if (current_node != new_node) {
            swap_nodes(tree, current_node, new_node);
            current_node = new_node;
        }
        current_node = tree->nodes[current_node].parent;
    }
}


static void add_new_node(tree_t *tree, int c) {
    int lightest_node = tree->next_free_node - 1;
    int new_node = tree->next_free_node;
    int zero_weight_node = tree->next_free_node + 1;
    tree->next_free_node += 2;

    tree->nodes[new_node] = tree->nodes[lightest_node];
    tree->nodes[new_node].parent = lightest_node;
    tree->leaf[tree->nodes[new_node].child] = new_node;

    tree->nodes[lightest_node].child = new_node;
    tree->nodes[lightest_node].child_is_leaf = false;

    tree->nodes[zero_weight_node].child = c;
    tree->nodes[zero_weight_node].child_is_leaf = true;
    tree->nodes[zero_weight_node].weight = 0;
    tree->nodes[zero_weight_node].parent = lightest_node;
    tree->leaf[c] = zero_weight_node;
}

static int encode_symbol(tree_t *tree, unsigned int c, compressed_file_t *output) {
    unsigned long code = 0;
    unsigned long current_bit = 1;
    int code_size = 0;
    int current_node = tree->leaf[c];

    if (current_node == -1)
        current_node = tree->leaf[ESCAPE];
    while (current_node != ROOT_NODE) {
        if ((current_node & 1) == 0)
            code |= current_bit;
        current_bit <<= 1;
        code_size++;
        current_node = tree->nodes[current_node].parent;
    }
    if (out_bits(output, code, code_size)) return 1;
    if (tree->leaf[c] == -1) {
        if (out_bits(output, (unsigned long) c, 8)) return 1;
        add_new_node(tree, (int) c);
    }
    return 0;
}


static int decode_symbol(tree_t *tree, compressed_file_t *input) {
    int current_node;
    int c;

    current_node = ROOT_NODE;
    while (!tree->nodes[current_node].child_is_leaf) {
        current_node = tree->nodes[current_node].child;
        current_node += in_bits(input, 1);
    }
    c = tree->nodes[current_node].child;
    if (c == ESCAPE) {
        c = (int) in_bits(input, 8);
        add_new_node(tree, c);
    }
    return c;
}

void compress_file(FILE *input, compressed_file_t *output) {
    tree_t tree;
    initialize_tree(&tree);
    int c;
    while ((c = getc(input)) != EOF) {
        if (encode_symbol(&tree, (unsigned int) c, output)) return;
        update_tree(&tree, c);
    }
    encode_symbol(&tree, END_OF_STREAM, output);
}


void expand_file(compressed_file_t *input, FILE *output) {
    tree_t tree;
    initialize_tree(&tree);
    int c;
    while ((c = decode_symbol(&tree, input)) != END_OF_STREAM) {
        if (putc(c, output) == EOF) {
            fprintf(stderr, "Error in expand_file");
            return;
        }
        update_tree(&tree, c);
    }

}

