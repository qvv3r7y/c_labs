#include <string.h>
#include "huffman.h"

static long file_size(char *stream) {
    long size = 0;
    FILE *file = fopen(stream, "rb");
    if (!file) return 0;
    fseek(file, 0, SEEK_END);
    size = ftell(file);
    fclose(file);
    return size;
}

static void print_ratio(char *in, char *out) {
    long int in_size = file_size(in);
    long out_size = file_size(out);
    int ratio;
    if (in_size == 0) return;
    ratio = 100 - (int) (out_size * 100 / in_size);
    printf("\nWeight old file:\t %ld \n", in_size);
    printf("Weight new file:\t %ld \n", out_size);
    printf("Ratio compress:\t\t%d%%\n", ratio);
}

static void print_info(char *in, char *out) {
    printf("\nAdaptive Huffman code - archive\n");
    printf("Compress file %s in %s", in, out);
    print_ratio(in, out);
}

int main(int argc, char *argv[]) {
    if (argc < 4) {
        fprintf(stdout, "A few arguments");
        return 0;
    }

    if (argv[1][0] == 'c') {
        compressed_file_t *output;
        FILE *input;
        input = fopen(argv[2], "rb");
        if (!input) {
            perror("\"Cannot open file");
            return 1;
        }
        output = open_for_bit_in_out(argv[3], OUT_STREAM);
        if (!output) {
            perror("\"Cannot open file");
            fclose(input);
            return 1;
        }
        compress_file(input, output);
        close_bit_in_out(output, OUT_STREAM);
        fclose(input);
        print_info(argv[2], argv[3]);
    }
    else if(argv[1][0] == 'd'){
        compressed_file_t *input;
        FILE *output;
        input = open_for_bit_in_out(argv[2], IN_STREAM);
        if (!input) {
            perror("\"Cannot open file");
            return 1;
        }
        output = fopen(argv[3], "wb");
        if (!output) {
            perror("\"Cannot open file");
            close_bit_in_out(input, IN_STREAM);
            return 1;
        }
        expand_file(input, output);
        close_bit_in_out(input, IN_STREAM);
        fclose(output);
    }
    return 0;
}

