
#ifndef LAB8_0_DSU_H
#define LAB8_0_DSU_H

#include <stdint.h>

typedef struct dsu_node {
    uint16_t parent;
    uint8_t rank;
} dsu_node_t;

void dsu_make_set(dsu_node_t *list, uint16_t vert);

uint16_t dsu_get_lead_set(dsu_node_t *list, uint16_t vert);

uint8_t dsu_union_sets(dsu_node_t *list, uint16_t a, uint16_t b);

#endif //LAB8_0_DSU_H
