
#ifndef LAB8_0_KRUSKAL_H
#define LAB8_0_KRUSKAL_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#define MAX_ERR_MSG 8
#define BAD INT_MAX + (unsigned int) 1

extern const char *err_msg[MAX_ERR_MSG];

enum err_code {
    QTY_VERT_ERR = 1,
    QTY_EDGE_ERR,
    IN_VERT_ERR,
    COST_ERR,
    QTY_LINES_ERR,
    BAD_GRAPH_ERR,
    ALLOC_ERR
};

typedef struct edge_node{
    unsigned int cost;
    uint16_t v1;
    uint16_t v2;
} edge_node_t;

uint8_t add_sort_edges(edge_node_t *list, uint32_t qty_edge, uint16_t qty_vert);

uint8_t make_spanning_tree(edge_node_t *edge_list, uint16_t qty_vert, uint32_t qty_edge);

#endif //LAB8_0_KRUSKAL_H
