#include "kruskal.h"

#define MAX_QTY_VERT 5000

static inline const char *get_err_msg(uint8_t code) {
    return code < MAX_ERR_MSG ? err_msg[code] : err_msg[0];
}

static void print_spinning_tree(const edge_node_t *edge_list, uint32_t qty_edge) {
    for (uint32_t i = 0; i < qty_edge; ++i) {
        if (edge_list[i].cost != BAD) printf("%hu %hu\n", edge_list[i].v1, edge_list[i].v2);
    }
}

int main(void) {
    uint8_t retcode = 0;
    uint16_t qty_vert = 0;
    uint32_t qty_edge = 0;
    if (scanf("%hu", &qty_vert) != 1) retcode = QTY_LINES_ERR;
    if (scanf("%u", &qty_edge) != 1) retcode = QTY_LINES_ERR;
    if (qty_vert > MAX_QTY_VERT) retcode = QTY_VERT_ERR;
    if (qty_edge > (uint32_t) (qty_vert * (qty_vert + 1) / 2)) retcode = QTY_EDGE_ERR;
    if (qty_vert == 0) retcode = BAD_GRAPH_ERR;
    if (retcode) {
        fprintf(stdout, "%s", get_err_msg(retcode));
        return 0;
    }

    edge_node_t *edge_list = calloc(qty_edge, sizeof(edge_node_t));
    if (!edge_list) {
        perror(get_err_msg(ALLOC_ERR));
        return ALLOC_ERR;
    }

    retcode = add_sort_edges(edge_list, qty_edge, qty_vert);
    if (retcode) {
        free(edge_list);
        fprintf(stdout, "%s", get_err_msg(retcode));
        return 0;
    }

    retcode = make_spanning_tree(edge_list, qty_vert, qty_edge);
    if (retcode) {
        free(edge_list);
        fprintf(stdout, "%s", get_err_msg(retcode));
        return 0;
    }

    print_spinning_tree(edge_list, qty_edge);
    free(edge_list);
    return retcode;
}

