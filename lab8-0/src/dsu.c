#include "dsu.h"

inline void dsu_make_set(dsu_node_t *list, uint16_t vert) {
    list[vert].parent = vert;
    list[vert].rank = 0;
}

uint16_t dsu_get_lead_set(dsu_node_t *list, uint16_t vert) {
    if (list[vert].parent == vert) return vert;
    else {
        list[vert].parent = dsu_get_lead_set(list, list[vert].parent);
        return list[vert].parent;
    }
}

uint8_t dsu_union_sets(dsu_node_t *list, uint16_t a, uint16_t b) {
    a = dsu_get_lead_set(list, a);
    b = dsu_get_lead_set(list, b);
    if (a == b) return 1;
    if (list[a].rank < list[b].rank) list[a].parent = b;
    else list[b].parent = a;
    if (list[a].rank == list[b].rank) ++list[a].rank;
    return 0;
}
