#include "kruskal.h"
#include "dsu.h"

const char *err_msg[MAX_ERR_MSG] = {
        "invalid error code",
        "bad number of vertices",
        "bad number of edges",
        "bad vertex",
        "bad length",
        "bad number of lines",
        "no spanning tree",
        "Allocation failure"
};

static int compare_edge(const void *v1, const void *v2) {
    return (int) (*(const edge_node_t *) v1).cost - (int) (*(const edge_node_t *) v2).cost;
}

uint8_t add_sort_edges(edge_node_t *list, uint32_t qty_edge, uint16_t qty_vert) {
    for (uint32_t i = 0; i < qty_edge; ++i) {
        if (scanf("%hu %hu %u", &list[i].v1, &list[i].v2, &list[i].cost) != 3) return QTY_LINES_ERR;
        if (list[i].v1 > qty_vert || list[i].v2 > qty_vert) return IN_VERT_ERR;
        if (list[i].cost > INT_MAX) return COST_ERR;
    }
    qsort(list, qty_edge, sizeof(edge_node_t), compare_edge);
    return 0;
}

static uint8_t _make_spanning_tree(edge_node_t *edge_list, dsu_node_t *dsu_list, uint16_t qty_vert, uint32_t qty_edge) {
    for (uint16_t i = 1; i <= qty_vert; ++i) {
        dsu_make_set(dsu_list, i);
    }
    for (uint32_t i = 0; i < qty_edge; ++i) {
        uint8_t cycle = dsu_union_sets(dsu_list, edge_list[i].v1, edge_list[i].v2);
        if (cycle) edge_list[i].cost = BAD;
    }
    uint16_t lead = dsu_get_lead_set(dsu_list, 1);
    for (uint16_t i = 2; i <= qty_vert; ++i) {
        if (dsu_get_lead_set(dsu_list, i) != lead) return BAD_GRAPH_ERR;
    }
    return 0;
}

uint8_t make_spanning_tree(edge_node_t *edge_list, uint16_t qty_vert, uint32_t qty_edge) {
    dsu_node_t *dsu_list = calloc(qty_vert + 1, sizeof(dsu_node_t));
    if (!dsu_list) return ALLOC_ERR;
    uint8_t err = _make_spanning_tree(edge_list, dsu_list, qty_vert, qty_edge);
    free(dsu_list);
    return err;
}
